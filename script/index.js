$(function(){
    var myaudio=document.getElementById("myaudio");
    var myvideo=document.getElementsByTagName("video");
    myaudio.play();
    $('.carousel-control, .carousel-indicators').click(function(){
        for(var i = 0; i < myvideo.length ; i++){
            myvideo[i].pause();
        }
    });
    for(var i = 0; i < myvideo.length ; i++){
        myvideo[i].addEventListener("playing", function () {
            audioStop();
        }, false);
    }
});
function audioStop(){
    var myaudio=document.getElementById("myaudio");
    if(!myaudio.paused) {
        myaudio.pause();
    }
}